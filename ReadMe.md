
# gosc

Pure Go (http://golang.org) support for OSC (http://opensoundcontrol.org)


## Install

`go get bitbucket.org/liamstask/gosc`


## Example
    :::go
        import (
    	    "bitucket.org/liamstask/gosc"
            "net"
        )
	
        m := &osc.Message{Address: "/my/message"}
        m.Args = append(m.Args, int32(12345))
        m.Args = append(m.Args, "important")
	
        // error checking omitted for brevity...
    	addr, _ := net.ResolveUDPAddr("udp", "127.0.0.1:12000")
    	conn, _ := net.DialUDP("udp", nil, addr)

    	numbytes, err := m.WriteTo(conn)
    	if err != nil {
    		// handle error
    	}
	
See the tests for more detailed usage.


## Status

Basic sending and receiving of OSC messages and bundles is well supported.

One major missing component is OSC server support - dispatching messages to registered listeners based on a message's address pattern.
